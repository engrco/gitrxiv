---
title: Natural Language Processing in Action    
subtitle: A One Book A Day Deep Dive
date: 2021-01-01
tags: ["text analysis", "OBADDD", "TBD0"]
---


[Natural Language Processing in Action](https://www.oreilly.com/library/view/natural-language-processing/9781617294631/) by Hobson Lane, Cole Howard, and Hannes Hapke (Manning Publications, 2019) is guide, from a different perspective and different publisher than the other books, to building machines that can read and interpret human language. It is about using readily available Python packages to capture the meaning in text and react accordingly. The book expands traditional NLP approaches to include neural networks, modern deep learning algorithms, and generative techniques as you tackle real-world problems like extracting dates and names, composing text, and answering free-form questions.

