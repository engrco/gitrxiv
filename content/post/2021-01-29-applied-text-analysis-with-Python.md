---
title: Applied Text Analysis with Python   
subtitle: A One Book A Day Deep Dive
date: 2021-01-01
tags: ["text analysis", "OBADDD", "TBD0"]
---


[Applied Text Analysis with Python](https://www.oreilly.com/library/view/applied-text-analysis/9781491963036/) by Benjamin Bengfort, Rebecca Bilbro, and Tony Ojeda (O’Reilly 2018)is about robust, repeatable, and scalable techniques for text analysis with Python, including contextual and linguistic feature engineering, vectorization, classification, topic modeling, entity resolution, graph analysis, and visual steering. By the end of this book, you’ll be equipped with practical methods to solve any number of complex real-world problems. 

    + Preprocess and vectorize text into high-dimensional feature representations
    + Perform document classification and topic modeling
    + Steer the model selection process with visual diagnostics
    + Extract key phrases, named entities, and graph structures to reason about data in text
    + Build a dialog framework to enable chatbots and language-driven interaction
    + Use Spark to scale processing power and neural networks to scale model complexity
