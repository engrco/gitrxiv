---
title: Mining the Social Web, 3rd Edition   
subtitle: A One Book A Day Deep Dive
date: 2021-01-01
tags: ["text analysis", "OBADDD", "TBD0"]
---



* [Mining the Social Web, 3rd Edition](https://www.oreilly.com/library/view/mining-the-social/9781491973547/) by Matthew A. Russell and Mikhail Klassen (O’Reilly, 2019) is about mining the rich data tucked away in popular social websites. With the third edition of this popular guide, data scientists, analysts, and programmers will learn how to glean insights from social media—including who’s connecting with whom, what they’re talking about, and where they’re located—using Python code examples, Jupyter notebooks, or Docker containers. In ***part one***, each standalone chapter focuses on one aspect of the social landscape, including each of the major social sites, as well as web pages, blogs and feeds, mailboxes, GitHub, and a newly added chapter covering Instagram. ***Part two*** provides a cookbook with two dozen bite-size recipes for solving particular issues with Twitter. With this text, you can:

    + Get a straightforward synopsis of the social web landscape
    + Use Docker to easily run each chapter’s example code, packaged as a Jupyter notebook
    + Adapt and contribute to the code’s open source GitHub repository
    + Learn how to employ best-in-class Python 3 tools to slice and dice the data you collect
    + Apply advanced mining techniques such as TFIDF, cosine similarity, collocation analysis, clique detection, and image recognition
    + Build beautiful data visualizations with Python and JavaScript toolkits

