---
title: Practical Natural Language Processing  
subtitle: A One Book A Day Deep Dive
date: 2021-01-01
tags: ["text analysis", "OBADDD", "TBD0"]
---


* [Practical Natural Language Processing](https://www.oreilly.com/library/view/practical-natural-language/9781492054047/) by Sowmya Vajjala, Bodhisattwa Majumder, Anuj Gupta, and Harshit Surana (O’Reilly, 2020) is the book to scan/read first if you want to build, iterate, and scale NLP systems in a business setting and tailor them for particular industry verticals, this is your guide. Software engineers and data scientists will learn how to navigate the maze of options available at each step of the journey. The book guides you through the process of building real-world NLP solutions embedded in larger product setups. You’ll learn how to adapt your solutions for different industry verticals such as healthcare, social media, and retail. You will:

    + Understand the wide spectrum of problem statements, tasks, and solution approaches within NLP
    + Implement and evaluate different NLP applications using machine learning and deep learning methods
    + Fine-tune your NLP solution based on your business problem and industry vertical
    + Evaluate various algorithms and approaches for NLP product tasks, datasets, and stages
    + Produce software solutions following best practices around release, deployment, and DevOps for NLP systems
    + Understand best practices, opportunities, and the roadmap for NLP from a business and product leader’s perspective

