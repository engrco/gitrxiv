---
title: Python Jupyter Notebooks with Colab   
subtitle: exploring machine learning, data analysis, education
date: 2021-05-05
tags: ["Jupyter", "Colab", "TBD0"]
---


This post started off being about the amazing history of Jupyter and as well as the amazing history of Python, the Python COSS world like Anaconda ... and as blog posts will do, this post got completely taken off its course by [Colab](https://colab.research.google.com/notebooks/intro.ipynb), Google Research efforts like [Tensorflow Research Cloud](https://sites.research.google/trc/) and the way that [Deep Mind has eaten Google](https://deepmind.com/blog).

[Colab](https://colab.research.google.com/notebooks/intro.ipynb) allows anybody to write and execute arbitrary python code through a hosted, no-setup Jupyter notebook service running in the browser. It provides free non-gauranteed flexible access to computing resources including GPUs, within [resource limits](https://research.google.com/colaboratory/faq.html#resource-limits) ... for FREE!

We ALL know that if something is free, we are the product, right? Well, maybe ... maybe we want to co-produce the same product.

[Colab](https://colab.research.google.com/notebooks/intro.ipynb) is especially well suited to playing around with machine learning, data analysis ... and thinking about education ... in other words, after spending some time with the free version of Colab, the account can be upgraded the $9.99/month [Colab Pro](https://colab.research.google.com/notebooks/pro.ipynb) ... which is nothing short of amazing ... really ... especially if one thinks back to a time in the not so distant past, when  doing things like the kinds of things you can do for $10/month would have set you back more than a brand new luxury roadster.

This is completely lost on 99.999% of the people out there ... in other words ... in the United States, there are fewer than 3,000 REALLY, REALLY pushing the limits with the use of Colab ... yes people try it, high schoolers and college kids might get assignments using Colab ... but it takes a while for skillsets to grow to REALLY push limits.

However, it is definitely NOT lost on everyone ... as the interest in [Google colaboratory tag on Stack Overflow](https://stackoverflow.com/questions/tagged/google-colaboratory) indicates, people are using Colab for some really interesting things ... and a few, within that magic 3,000 are doing things like [training PyTorch XLA models on ALL 8 CORES of a Colab TPU](https://rishtech.substack.com/p/xla-tpu) ... but experience will grow ... some will use the non-gaurarantee flexible free Colab and *get it* ... because it increasingly [apparent that if you want to learn to do deep learning SERIOUSLY](https://analyticsindiamag.com/why-google-decided-to-launch-a-paid-colab-pro/), you will have to invest in GPUs without resource limits, whether it is through cloud services, subscription models like Colab Pro, or purchasing your own GPUs ... and the **experience** with SERIOUS computing power delivering results will be noticed ... and then, within a few years, become ROUTINE.

Because that's how it'll work with skills now ... LEARNING new, more power technologies and early, successful RESULTS ... will be necessary to be employed, ie not college degrees. 

# The business model of the ivory towers is toast.