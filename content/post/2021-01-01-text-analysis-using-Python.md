---
title: Blueprints For Text Analysis Using Python    
subtitle: A One Book A Day Deep Dive
date: 2021-01-01
tags: ["text analysis", "OBADDD", "TBD0"]
---


Turning text into valuable information is essential for any serious rXivist attempting to find and possibly network with others who are involved in creating especially valuable information. Text analytics is about automation of the more tedious parts of the task that involves pre-analyzing or screening vast amounts of information which available in the form of text. 

So today one our OBAD (One Book a Day Deep Dive) we point our diver toward [Blueprints for Text Analytics Using Python](https://learning.oreilly.com/library/view/blueprints-for-text/9781492074076/) which is a serious, practical, nuts-and-bolts daily business blueprint book intended for data scientists and data devops entering the area of text analytics and natural language processing. Thus, we put the focus on developing practical solutions that can serve as blueprints in your daily business.  -- for those seeking additional background, you might want to look the author's blogs and/or twitterfeed to furnishes some additional insights into their thinking -- but you will find this book extremely practical and to-the-point.

The authors suggestion to have the following books to read alongside this one is highly recommended; in fact, you probably should devote an OBADDD to these materials in order to really apply or possibly extend/modify the blueprints in this texts.  The highly suggested, almost mandatory texts, are 

* [Practical Natural Language Processing](https://www.oreilly.com/library/view/practical-natural-language/9781492054047/) by Sowmya Vajjala, Bodhisattwa Majumder, Anuj Gupta, and Harshit Surana (O’Reilly, 2020) is the book to scan/read first if you want to build, iterate, and scale NLP systems in a business setting and tailor them for particular industry verticals, this is your guide. Software engineers and data scientists will learn how to navigate the maze of options available at each step of the journey. The book guides you through the process of building real-world NLP solutions embedded in larger product setups. You’ll learn how to adapt your solutions for different industry verticals such as healthcare, social media, and retail.

* [Natural Language Processing in Action](https://www.oreilly.com/library/view/natural-language-processing/9781617294631/) by Hobson Lane, Cole Howard, and Hannes Hapke (Manning Publications, 2019) is guide, from a different perspective and different publisher than the other books, to building machines that can read and interpret human language. It is about using readily available Python packages to capture the meaning in text and react accordingly. The book expands traditional NLP approaches to include neural networks, modern deep learning algorithms, and generative techniques as you tackle real-world problems like extracting dates and names, composing text, and answering free-form questions.

* [Mining the Social Web, 3rd Edition](https://www.oreilly.com/library/view/mining-the-social/9781491973547/) by Matthew A. Russell and Mikhail Klassen (O’Reilly, 2019) is about mining the rich data tucked away in popular social websites. With the third edition of this popular guide, data scientists, analysts, and programmers will learn how to glean insights from social media—including who’s connecting with whom, what they’re talking about, and where they’re located—using Python code examples, Jupyter notebooks, or Docker containers. In ***part one***, each standalone chapter focuses on one aspect of the social landscape, including each of the major social sites, as well as web pages, blogs and feeds, mailboxes, GitHub, and a newly added chapter covering Instagram. ***Part two*** provides a cookbook with two dozen bite-size recipes for solving particular issues with Twitter. 

* [Applied Text Analysis with Python](https://www.oreilly.com/library/view/applied-text-analysis/9781491963036/) by Benjamin Bengfort, Rebecca Bilbro, and Tony Ojeda (O’Reilly 2018)is about robust, repeatable, and scalable techniques for text analysis with Python, including contextual and linguistic feature engineering, vectorization, classification, topic modeling, entity resolution, graph analysis, and visual steering. By the end of this book, you’ll be equipped with practical methods to solve any number of complex real-world problems. 

* [Python for Data Analysis, 2nd Edition](https://www.oreilly.com/library/view/applied-text-analysis/9781491963036/) by Wes McKinney (O’Reilly, 2017) is a practical, modern introduction to data science tools in Python. It’s ideal for analysts new to Python and for Python programmers new to data science and scientific computing. Data files and related material are available on GitHub.

AFTER you have prepared yourself by maybe at least reading the Tables of Content and scanning the topics in your area of interest in these books ... and probably also refreshing your memory of some the key Python methods and libraries [if you don't already work in this material every day or so], you will ready to get the most out of this text.

## Discussion of Contents

### 1. Gaining Early Insights from Textual Data

If you only have time, at first, to read just one chapter of any book, read The first one, as a general rule. That rule is especially for blueprint books, like this. Read the first chapter and even re-read it carefully -- it does an excellent job of furnishing *overall lay of the land*.  Of course, also provides a good indicator of what other specific blueprints in the book  [for your purposes] that you might want jump ahead to now and at least skim [for insight specifically applicable to your needs], probably coming back later to apply. 

The [Gaining Early Insights from Textual Data chapter](https://learning.oreilly.com/library/view/blueprints-for-text/9781492074076/ch01.html) covers getting started with the statistical exploration of textual data. Exploratory data analysis is the process of systematically examining data on an aggregated level. The first blueprint uses use only metadata and record counts to explore data distribution and quality; it't not YET a looking at textual content, instead just very quick exploration, keeping it simple, to the explore the most with minimal set-up, ie calculating summary statistics, checking for missing values, plotting and comparing attribute distributions of interesting attributes and seeing developments over time. The simple data processing pipeline presented in the second blueprint is just three steps: case-folding into lowercase, tokenization, and stop word removal. The next blueprints developed calculate and visualize word frequencies with counters, frequency diagrams, word clouds,  ... all of the following chapters builds on this first one with blueprints for frequency analysis, inverse document frequency, keyword-in-context (KWIC) analysis, N=Gram analysis and ways of comparing frequencies across time intervals and categories.


### 2. Extracting Textual Insights with APIs

The [Extracting Textual Insights with APIs chapter](https://learning.oreilly.com/library/view/blueprints-for-text/9781492074076/ch02.html) is about using different Python modules to extract data from popular APIs. Using APIs is the strongly preferred approach over scraping a website. Read the API documentation carefully; then re-read it to really get the details ... not just on rate limits to but really get inside the mindest or internalize the [data exchange architecture of the API](https://www.redhat.com/architect/apis-soap-rest-graphql-grpc)-- it might SOAP, REST, GraphQL, gRPC ... and not all architectures will be equal -- it's THEIR data API; read THEIR documentation to understand how your Python will interact with THEIR data API. This chapter provides what are probably the best examples for beginners to gain confidence and learn. [Github's REST API](https://docs.github.com/en/rest) and [Github's GraphQL API](https://docs.github.com/en/graphql) are good for learning how to use EITHER. [Tweepy](https://docs.tweepy.org/) is example of what some companies do to really advance the use of their data, but not all companies/organizations/governments will yet be this accomodating ...

### 3. Scraping Websites and Extracting Data

The [Scraping Websites and Extracting Data chapter](https://learning.oreilly.com/library/view/blueprints-for-text/9781492074076/ch03.html) discusses the use of Python libraries to download web pages and extract content.

### 4. Preparing Textual Data for Statistics and Machine Learning

The [Preparing Textual Data for Statistics and Machine Learning chapter](https://learning.oreilly.com/library/view/blueprints-for-text/9781492074076/ch04.html) is a superficial introduction to data cleaning and [the preparations necessary for] linguistic processing.

#### NOTE

*If you can see your path laid out in front of you step by step, you know it’s not your path. Your own path you make with every step you take. That’s why it’s your path.* -- Joseph Campbell

If you want your path to be on a solid footing -- please go back, re-read the first four chapters and perhaps dive deeper into PREPARING THE DATA ... or else you will be one of those immature souls who builds one of those annoying automated GIGO clusterfucks in which people are excited to get the crayons out of the box and draw a pretty picture for Grandma ... the rest of this is in "Knowing Just Enough To Build Something Dangerous" category. 

### 5. Feature Engineering and Syntactic Similarity

The [Feature Engineering and Syntactic Similarity chapter](https://learning.oreilly.com/library/view/blueprints-for-text/9781492074076/ch05.html) is an introduction to features and vectorization.

### 6. Text Classification Algorithms

The [Text Classification Algorithms chapter](https://learning.oreilly.com/library/view/blueprints-for-text/9781492074076/ch06.html) uses machine learning algorithms to classify software bugs.

### 7. How to Explain a Text Classifier

The [How to Explain a Text Classifiers chapter](https://learning.oreilly.com/library/view/blueprints-for-text/9781492074076/ch07.html) explains models and classification results.

### 8. Unsupervised Methods: Topic Modeling and Clustering

The [Unsupervised Methods: Topic Modeling and Clustering chapter](https://learning.oreilly.com/library/view/blueprints-for-text/9781492074076/ch08.html) shows how to use unsupervised methods to gain unbiased insights into text.

### 9. Text Summarization

The [Text Summarization chapter](https://learning.oreilly.com/library/view/blueprints-for-text/9781492074076/ch09.html) illustrates how to create short summaries of news articles and forum threads using rule-based and machine learning approaches.

### 10. Exploring Semantic Relationships with Word Embeddings

The [Exploring Semantic Relationships with Word Embeddings chapter](https://learning.oreilly.com/library/view/blueprints-for-text/9781492074076/ch10.html) teaches you how to use word embeddings to explore and visualize semantic similarities in a specific data set.

### 11. Performing Sentiment Analysis on Text Data

The [Performing Sentiment Analysis on Text Data chapter](https://learning.oreilly.com/library/view/blueprints-for-text/9781492074076/ch11.html) takes you through the process of identifying customer sentiment in Amazon product reviews.

### 12. Building a Knowledge Graph

The [Building a Knowledge Graph chapter](https://learning.oreilly.com/library/view/blueprints-for-text/9781492074076/ch12.html) shows how to extract named entities and their relationships using pretrained models and custom rules.

### 13. Using Text Analytics in Production

The [Using Text Analytics in Production chapter](https://learning.oreilly.com/library/view/blueprints-for-text/9781492074076/ch13.html) illustrates how to deploy and scale the sentiment analysis blueprint as an API on Google Cloud Platform.