---
title: GraphQL vs REST vs SOAP; GraphQL is the better REST which is better than SOAP 
subtitle: GraphQL in Action
date: 2021-01-01
tags: ["reading list", "OBADDD", "TBD0"]
---


This mini-deep-dive exploration of [GraphQL in Action](https://learning.oreilly.com/library/view/graphql-in-action/9781617295683/) ... along with some of the SOAP and REST or how-we-got-here historical references ... grew out of by a simple little paragraph in [Blueprints for Text Analytics Using Python](https://learning.oreilly.com/library/view/blueprints-for-text/9781492074076/) The technology used to implement APIs has changed over the years, but it is extremely important for us to understand why that change happened to avoid indefensibly repeating the defensible unforeseen-at-the-time errors of past implementation technologies. GitHub, for example, might be thought of as one of part of the generation of programmers leading the charge away, ie "fleeing the burning building" might be appropriate visual image, from SOAP to REST -- Github maintains version three of its APIs as a REST API and a fourth GraphQL API that the authors of the Blueprints For Text Analytics text uses  will use in the blueprint, whereas the latest version four is a GraphQL API.

* Simple Object Access Protocol (SOAP) was one of the earliest methods for different software modules to speak with one another using standard interfaces. SOAP uses a standard messaging format encapsulated using the Extensible Markup Language (XML) and can use any communication protocol (like HTTP, TCP) to transmit the message. SOAP provided lots of opportunities for learning, ie pain, when it came to API gateway support because the extremely flexible declarative XML format so SOAP specifications tended unfortunately to be really vague and left a lot of things open for interpretation.

* Representational State Transfer (REST), on the other hand, relies on HTTP as the communication protocol including the use of status codes to determine successful or failed calls. It defines data types much more loosely and uses JSON heavily, though other formats are also supported. SOAP is generally considered an older protocol and is typically used within legacy applications in large enterprises, while REST is a preferred format adopted by several web-based services. While GraphQL has gained popularity since it was open sourced by Facebook in 2015, REST APIs are much more common as of early 2021. 

* [Graph Query Language (GraphQL)](https://graphql.org/) with its the GraphQL Schema Definition Language (SDL) are relatively new (publicly released in 2015 after internal development in from Facebook starting in 2012) open source data query and manipulation language for APIs as well as the [GraphQL IDE Monorepo & the GraphQL LSP Reference Ecosystem](https://github.com/graphql/graphiql/tree/main/packages/graphiql#readme) for building browser & IDE tools. The specification defines a way to interact with APIs similar to writing SQL queries. One of the drawbacks of the REST architecture is that retrieving a single piece of information might require multiple calls to different resources. This will depend on how the resources are organized; for instance, to determine whether a user’s phone number is active, one would have to make an API call to the /user endpoint to retrieve all details followed by a subsequent call to a different endpoint like /contacts with the phone number to check whether that phone number is active. In GraphQL this would be a single API call with a specific SQL-like query where all active phone numbers of a given user would be retrieved. 

At this current time, REST remains an extremely popular or established schema-optional "standard" way to expose data with an API from a server at this time ... but since [GraphQL is the better REST](https://www.howtographql.com/basics/1-graphql-is-the-better-rest/) that is changing as more people understand the importance of the GraphQL schema. When the concept of REST was developed, client applications were relatively simple and the expectations of the speed of development were not as great. REST thus has been a very good fit for many applications. However, the API landscape has radically changed over the last couple of years. In particular, there are three factors that have been challenging the way APIs are designed:

1. The demands of mobile usage drive the need for efficient data loading
Increased mobile usage, low-powered devices and sloppy networks were the initial reasons why Facebook developed GraphQL. GraphQL minimizes the amount of data that needs to be transferred over the network and thus majorly improves applications operating under these conditions.

2. Variety of different frontend frameworks and platforms
The heterogeneous landscape of frontend frameworks and platforms that run client applications makes it difficult to build and maintain one API that would fit the requirements of all. With GraphQL, each client can access precisely the data it needs without overfetching or underfetching.

3. Fast development & expectation for rapid feature development
Continuous deployment has become a standard for many companies, rapid iterations and frequent product updates are indispensable. With REST APIs, the way data is exposed by the server often needs to be modified to account for specific requirements and design changes on the client-side. This hinders fast development practices and product iterations.

The REST standard is fuzzy but because of that fuzziness; the variety of different REST APIs create situation for developers that is requires lots of specific implementation details, making systems difficult to maintain, even too inflexible to keep up with the rapidly changing requirements of the clients that access them. GraphQL was developed to cope with the need for more flexibility and efficiency of the downstream clients. GraphQL solves many of the shortcomings and inefficiencies that developers experience when interacting with schemaless REST APIs. A GraphQL schema is the backbone of every GraphQL API, but every GraphQL API is ALSO a RESTful API. The GraphQL schema clearly defines the operations (queries, mutations and subscriptions) supported by the API, including input arguments and possible responses. The schema is an unfailing contract that specifies the capabilities of an API. GraphQL schemas are strongly-typed and can be written in the simple and expressive GraphQL Schema Definition Language (SDL). Thanks to this strong type-system, developers benefit from a known schema in ways that are just not even *on the table* with schemaless APIs.

**Thus, [GraphQL is the better REST](https://www.howtographql.com/basics/1-graphql-is-the-better-rest/).**  GraphQL's evolution has taught its developers a lot about what's important when designing APIs and those lessons are not reflected in what are best practices GraphQL users and language designers. Obvious and self-documenting naming matters. Think in whole, cohesive graphs, not just endpoints. Describe the data, not the view. GraphQL is a thin interface sitting atop your existing and future systems -- keep it thin. Hide implementation details -- GraphQL should be a thin seam between data and the details of its storage and retrieval. 

There are also a few general principles for any software developer to follow when building a new product.  
* **Solve a real problem.** For GraphQL, the problem was getting the data needed for the Facebook news feed on iOS devices over poor mobile network connections. This was an extremely pressing issue for Facebook as more and more daily users accessed the service on mobile devices. Identifying the real problem helps keep you grounded about which features to prioritize.
* **Think like the client.** The topmost priorities of GraphQL's developers were the needs of the end user. Figure out who will be using the technology and what they want to accomplish. Just because there are existing practices or tools out there doesn't mean that they're the best solution for your particular problem.
* **Have a first client.** Whenever you design something new, it's easy to start considering all the possible future use cases and accommodate them all up front. However, even the best predictions can go wrong and result in poor tradeoffs and unnecessary complexity. GraphQL's first client was the Facebook news feed for iOS, and those needs and priorities drove how it was initially designed.
* **Incremental adoption.** Make it easy for your first client to replace their endpoints, resources or data models one by one. This allows them to try out new ideas and to evaluate the strengths and weaknesses of your product during the adoption process, instead of migrating to the whole thing immediately.
* **YAGNI (You Aren't Gonna Need It)**. Avoid implementing things that you might need in the future in favor of things that you definitely need today. When you consider adding a feature, ask yourself if it can be accomplished with the tools and features that you already have. Evaluate if the disadvantages of your existing system are preferable to the cost of developing a new feature.
* **Avoid "second system syndrome."** Fred Brooks talks about this in his classic software engineer book "The Mythical Man-Month." The risk is that when you build a new thing for the second time, you'll feel less cautious and more experienced, and end up over designing the whole thing. The best way to combat this is to have someone who can play devil's advocate for you and provide a second opinion.
* **Timing is critical; recognize inflection points.** Making changes to your product is easier at the beginning when you're working closely with your first client. Once it sees wider adoption, it becomes significantly harder to change. Consider making your code base open-source only when your product is older and needs less freedom to rapidly evolve.
* **Encourage taking measured risks.** When you're solving an important problem, it's easy to be risk-averse and stick to what you know. However, managers and teams should be able to trust experienced engineers to decide when certain risks are worth taking and to be accountable for those decisions. If your strongest engineers are telling you that it's time for something new, give their thoughts a listen.



To explore the [GraphQL topic](https://learning.oreilly.com/topics/graphql/), it is perhaps worth starting with a miniature deep dive into [GraphQL in Action](https://learning.oreilly.com/library/view/graphql-in-action/9781617295683/)

1) Introduction to GraphQL

The most important question to ask is WHY does anyone need GraphQL? It's necessary to understand from first principles, why GraphQL, from the initial assumptions about the problem to the conceptual to the specification to the langauge to the runtime services ... is all about thinking about precisely what different clients will NEED ... and then, it's a matter of optimizing data communication between any client and an data API server to efficiently, effectively, quickly meet that exact need.  

The best way to represent data in the real world is with a graph-like data structure ... instead of thinking of data in terms of implementation details such as a hyperlink to some resource or tables of rows and columns of values in a database. When you can see data as its place in "the big picture" for the user it's about context and the best way to represent that big picture context is probably going to be a **graph**. 

Thus, GraphQL is a declarative, flexible, efficient query language for data APIs. The frontend data API perspective is the GraphQL language layer data in which consumers of the data API the use the GraphQL language to request the data they need. The frontend GraphQL language is designed to be close to how one should picture or think data in their heads rather than how the data is stored or how data relations are implemented on the backend with GraphQL-based stack. The GraphQL-based stack needs a runtime to provide a structure or schema for the servers to describe their data to be exposed in their APIs. 

In practice, the frontend client uses the GraphQL language to construct a text request representing their exact data needs and sends that text request to the API service through a transport channel, eg https. The GraphQL runtime layer accepts the text request, communicates with other services in the backend stack to put together a suitable data response, and then sends that data back to the consumer in a format like JSON. JSON is a popular language for communicating data from API servers to client applications. Most of the modern data API servers use JSON to fulfill the data requirements of client applications. GraphQL is not specific to any backend or frontend framework, technical stack, or database. It can be used in any frontend environment, on any backend platform, and with any database engine. You can use it on any transport channel and make it use any data representation format. 

In web or mobile applications, frontend developer could use GraphQL by making direct Ajax calls to a GraphQL server or with a client like Apollo or Relay [which will make the Ajax request on the developers behalf). They can use a library like React or React Native to manage the application's views and its use the data coming from a GraphQL service. Developers can also do that with APIs native to their UI environments, eg DOM API, native iOS components. Although a frontend developer does not need something like React, Apollo, or Relay to use GraphQL in your applications, these libraries add more value immediately on just using GraphQL APIs without having to do complex data management tasks.

In general, an API is an interface that enables communication between multiple components in an application. GraphQL is an evolving language, but the specification document was a genius start for the project because it defined standard rules and practices that all implementers of GraphQL runtimes must adhere to. You can ultimately find everything about the GraphQL language and runtime requirements in the official specification document. GraphQL is a language be used for both reading data, with queries, and modifying data with mutations. Queries represent READ operations. Mutations represent WRITE-then-READ operations, ie mutations are queries with side effects. In addition to queries and mutations, GraphQL also supports a third request type called a subscription, used for real-time data monitoring requests. Subscriptions represent continuous READ operations. Mutations usually trigger events for subscriptions. GraphQL subscriptions require the use of a data-transport channel that supports continuous pushing of data. That’s usually done with WebSockets for web applications.

A GraphQL runtime layer is implemented on the backend and exposed to the clients that want to communicate with the service. When a client application speaks the GraphQL language, it directly communicates specific data requirements to a backend data service which also speaks GraphQL. The backend runtime layer on the server side is effectively GraphQL translator or a GraphQL-speaking agent that represents the data service.  GraphQL is not a storage engine, so it cannot be a solution on its own -- it takes more than just the backend GraphQL translator on the server speaking GraphQL to the front-end; it is necessary to implement a translating runtime layer or service. That GraphQL service can be written in any programming language. It is conceptually split into two major parts, structure and behavior. The structure or GraphQL schema is often compared to a restaurant menu. In that analogy, the waitstaff or table servers behave like instances of the GraphQL API interface, taking orders back to the kitchen, which is the core of the API service. 

**Whether we are talking about enjoyable jazz improvization or safety-critical engineering design or efficient ways to expose data APIs for uses that have even been concieved of yet,  critically-important communication ALWAYS, ALWAYS, ALWAYS boils down to standards in order for data to have any context ... otherwise, the data are just dangerous noise.** In summary, the answer to the big WHY question is that GraphQL provides comprehensive standards and structures to implement API features in maintainable and scalable ways. GraphQL makes it mandatory for data API servers to publish documentation, ie the data API's schema, about the data API's capabilities. That schema enables client applications to know everything available for them on these servers. The GraphQL standard schema has to be part of every GraphQL data API. Clients communicate with the service in the context of the data API's GraphQL schema using the GraphQL language.

The biggest relevant problem with REST APIs is the client’s need to communicate with multiple data API endpoints performing multiple network requests to that REST API and then put together the data by combining the multiple responses it receives ... so REST APIs servers require clients to do multiple network round trips to underfetch and overfetch data to iteratively get the data the client needs. Furthermore, in a REST API, there is no client request language. Clients do not have control over what data the server will return because they do not have a language to communicate their exact needs. In a pure REST API (not a customized one), a client cannot specify which fields to select for a record in that resource. That information is in the REST API service itself, and the REST API service always overfetches or returns all the fields regardless of which ones the client actually needs ... so the client might start out by underfetching to get a subset of the data to more quickly give the user *something*.

The GraphQL way is a typed graph schema, a declarative language and single endpoint for the client which uses data-centric client langauge which about the data the client needs ... *as opposed to a partially server-driven langauge that involves knowing something about servers, storage engines and backend implementation details in order come up with a way of asking for the needed data.*  The GraphQL way is to avoid versioning of the data API altogether ... to always be backward compatible, to grow flexibly by adding nodes, to leave paths on the graph for old APIs and to introduce/add new ones.

There's no such thing as a free lunch. The flexibility that GraphQL introduces opens a door to some clear issues and concerns ... **security**, eg DDoS attacks -- A GraphQL server can be attacked with overly complex queries that consume all the server resources ... **caching and optimizing** -- optimally, a graph query means a graph cache, which means managing a cyclic graph and a query traversal and probably a separate layer to handle the cache logic, ie not exactly a simple process ... the **learning curve** for any new way of thinking about something is always tough [which especially matters when the established way, eg REST APIs, seem to working kinda sorta pretty much *good enough* in most cases and the people involved in data APIs will have other pressing tasks.].

2) Exploring GraphQL APIs

GraphiQL is an open source web application (written with React.js and GraphQL) that can be downloaded, but it might be best if [run in a browser](https://graphql.org/swapi-graphql/). to just jump into writing and testing GraphQL requests. It offers many great features to write, validate, and inspect GraphQL queries and mutations. These features are made possible thanks to GraphQL’s introspective nature, which comes with its mandatory schemas.

A GraphQL request consists of a set of operations, an object for variables, and other meta-information elements as needed.

GraphQL operations use a tree of fields. A field represents a unit of information. The GraphQL language is largely about selecting fields on objects.

GitHub has a powerful GraphQL API that you can use to read data about repositories and users and do mutations like adding a star to a repository or commenting on an issue in a repository.

GraphQL introspective queries offer a way for clients to get meta-information about the GraphQL API.

3) Customizing and organizing GraphQL operations

You can pass arguments to GraphQL fields when sending requests. GraphQL servers can use these arguments to support features like identifying a single record, limiting the number of records returned by a list field, ordering records and paginating through them, searching and filtering, and providing input values for mutations.

You can give any GraphQL field an alias name. This enables you to customize a server response using the client’s request text.

You can use GraphQL directives to customize the structure of a GraphQL server response based on conditions in your applications.

Directives and field arguments are often used with request variables. These variables make your GraphQL requests reusable with dynamic values without needing to resort to string concatenation.

You can use fragments, which are the composition units of GraphQL, to reuse common parts of a GraphQL query and compose a full query by putting together multiple fragments. This is a winning strategy when paired with UI components and their data needs. GraphQL also supports inline fragments that can be used to conditionally pick information out of union object types or object types that implement an interface.

4) Designing a GraphQL schema

An API server is an interface to one or many data sources. GraphQL is not a storage engine; it’s just a runtime that can power an API server.

An API server can talk to many types of data services. Data can be queried from databases, cache services, other APIs, files, and so on.

A good first step when designing a GraphQL API is to draft a list of operations that will theoretically satisfy the needs of the application you’re designing. Operations include queries, mutations, and subscriptions.

Relational databases (like PostgreSQL) are a good fit to store relations and well-defined, constrained data. Document databases (like MongoDB) are great for dynamic data structures.

Draft GraphQL operations can be used to design the tables and collections in databases and to come up with the initial GraphQL schema language text.

You should utilize the powerful data-integrity constraints and schema validators that are natively offered by database services.


5) Implementing schema resolvers

A GraphQL service is centered around the concept of a schema that is made executable with resolver functions.

A GraphQL implementation like GraphQL.js takes care of the generic tasks involved in working with an executable schema.

You can interact with a GraphQL service using any communication interface. HTTP(S) is the popular choice for GraphQL services designed for web and mobile applications.

You can convert from one schema representation to another using GraphQL.js helper functions like buildSchema and printSchema.

You should not do long-running processes synchronously because doing so will block a GraphQL service process for all clients. Resolver functions in the GraphQL.js implementation can work with asynchronous promise-based operations out of the box.


6) Working with database models and relations

Use realistic, production-like data in development to make your manual tests relevant and useful.

Start with the simplest implementations you can think of. Make things work, and then improve on your implementations.

You can use the GraphQL context object to make a pool of database connections available to all resolver functions.

You can use fields’ resolvers to transform the names and values of your data elements. The GraphQL API does not have to match the structure of the data in the database.

Try to separate the logic for database interactions from other logic in your resolvers.

It’s a good practice to wrap third-party APIs with your own calls. This gives you some control over their behavior and makes the code a bit more maintainable going forward.

Resolving database relations involves issuing SQL statements over many tables. This causes an N+1 queries problem by default because of the graph-resolving nature of GraphQL. We can solve this problem using database views, but that complicates the code in the GraphQL service. In the next chapter, we will learn about the DataLoader library, which offers a better way to deal with the N+1 problem and make your GraphQL service more efficient in general.


7) Optimizing data fetching

To optimize data-fetching operations in a generic, scalable way, you can use the concepts of caching and batching. You can cache SQL responses based on unique values like IDs or any other custom unique values you design in your API service. You can also delay asking the database about a specific resource until you figure out all the unique IDs of all the records needed from that resource and then send a single request to the database to include all the records based on all the IDs.

DataLoader is a generic JavaScript library that can be used as part of your application’s data-fetching layer to provide a simplified, consistent API over various data sources and abstract the use of batching and caching. This enables you to focus on your application’s logic and safely distribute its data-fetching requirements without worrying about maintaining minimal requests to your databases and other sources of data.

DataLoader instances are meant to be scoped for a single request. They can be used for both ID-based SQL statements and more complex statements like fetching lists or even full-text-search requests.

If you use DataLoader in your stack, it’s a good practice to do all database communications through it. Your GraphQL resolvers will then delegate the task of resolving the data to the DataLoader instances. This makes the code cleaner and more maintainable.


8) Implementing mutations

To host mutations, a GraphQL schema must define a root mutation type.

To organize database operations for mutations, you can group them on a single object that you expose as part of the global context for resolvers.

User-friendly error messages can and should be included as part of any mutation response.

The PostgreSQL RETURNING clause can be used to do a WRITE operation followed by a READ operation in a single SQL statement. The INSERT/SELECT combination enables us to make a WRITE operation depend on a READ operation in a single SQL statement.

A hashed token value can be used as a temporary password for mutation operations and query operations that should behave differently for authenticated users.

Some mutation operations depend on each other. Dependency injection can be used in these cases.

9) Using GraphQL APIs without a client library

To use a GraphQL operation in a frontend web application, you need to make Ajax calls. These calls usually cause the UI state to change. You’ll have to make them in a place where it’s possible to read and update the state of the UI.

Components can define operations and fragments. An operation can be a query, a mutation, or a subscription. A component query operation is generally used to display that component. A mutation operation is usually used within a DOM event handler (like onSubmit or onClick). A subscription operation is generally used to autoupdate a frequently changed view.

Fragments allow components to be responsible for their own data requirements. Parent components can use these fragments to include the data required by their children components.

Mutation operations usually require the reading of input element values. The structure of a UI form should match the structure of the mutation input type.

GraphQL introspective queries can be used to future-proof parts of the UI that depend on dynamic values.

10) Using GraphQL APIs with Apollo client

A GraphQL client library like Apollo manages all the communications between a frontend application and its GraphQL API service. It issues data requests and makes their data responses available where needed.

You can use Apollo Client with plain JavaScript or with view libraries like React, Vue, and Angular. For React, Apollo Client provides custom hook functions that greatly simplify the code in function components.

Apollo has a powerful caching store that’s designed to work with GraphQL’s graph-like structure. The cache works automatically in common cases, but you can also manually read it and modify it. This cache, combined with other features in Apollo, can replace local app state management tools (like React’s context or Redux).

Apollo Client is flexible. It offers many methods to invoke queries on render, on demand, or conditionally. You can use different types of caching stores. You can modify request headers globally or per operation. You can skip queries and instruct Apollo not to cache them if needed. You can have it communicate with multiple GraphQL services and use the same store for them. You can even use it to communicate with REST-based API services.

GraphQL subscriptions are great for incremental real-time data. To use subscription operations in web applications, a GraphQL server has to support WebSockets and a Pub/Sub messaging pattern. Apollo Server is an example of a GraphQL implementation that has that support. On the frontend, a client has to determine what communication channel to use based on the operation and use a WebSocket-based link for subscriptions.