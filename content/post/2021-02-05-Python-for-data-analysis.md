---
title: Python for Data Analysis, 2nd Edition   
subtitle: A One Book A Day Deep Dive
date: 2021-01-01
tags: ["text analysis", "OBADDD", "TBD0"]
---


[Python for Data Analysis, 2nd Edition](https://www.oreilly.com/library/view/applied-text-analysis/9781491963036/) by Wes McKinney (O’Reilly, 2017) is a practical, modern introduction to data science tools in Python. It’s ideal for analysts new to Python and for Python programmers new to data science and scientific computing. Data files and related material are available on GitHub.

    + Use the IPython shell and Jupyter notebook for exploratory computing
    + Learn basic and advanced features in NumPy (Numerical Python)
    + Get started with data analysis tools in the pandas library
    + Use flexible tools to load, clean, transform, merge, and reshape data
    + Create informative visualizations with matplotlib
    + Apply the pandas groupby facility to slice, dice, and summarize datasets
    + Analyze and manipulate regular and irregular time series data
    + Learn how to solve real-world data analysis problems with thorough, detailed examples

