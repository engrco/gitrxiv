---
title: It's the Peer-Review, Stupid
subtitle: PapersWithCode, GitPod, Theia, QwikLabs, Codelabs, Sandboxes
date: 2021-07-05
tags: ["Colab", "TBD0"]
---

Let's say for a second that we actually wanted to TRAIN people ... rather than SUCKER them on paying for the appearance of being credentialled.

When we want to train people, we have to get them the resources and opportunities to fail ... but to fail with no risk, in a way that they can recover ... but it might be better if the failures can occur in public so that others watching can also learn. But there's more to it than that ... we can and must certainly practice in-private and we might workout our moves in quasi-private with close buddies, but ... 

When we are ready, we need to *get up in front of the class and perform.*  That matters because we are humans ... with egos ... and humility is a bit uncomfortable.  So failing on-stage is a form of hormesis, it gives us humility AND it makes us stronger. Public learning, like public speaking is tough -- it's like heat-treating steel, ie the steel doesn't have to like the exposure to heat, but without the public exercise, there's no good way to harden or *set* the lesson. 

Increasingly, in the virtual realm, these resources and opportunities are available for free or at extremely low cost ... there are open source communities surrounding these resources in which we can dogfood the project management and development frameworks themselves in order to add more resources, more opportunities ... more directed tutorial codelabs, more provisioned sandboxes [so that students can get started, learning the most basic lessons RATHER than *getting shot down before they start* because of some failure in provisioning to basic starter environment for taking on the most basic lesson]

# The business, education and research models of the ivory towers are toast.

People LEARN by doing, failing, arguing ... and eventually succeeding and then explaining their lesson to others. Until we can explain what we have done to others, we have not really learned something. Learning might involved sub-tasks like reading or memorizing ... but someone is not considered learned, because they have memorized the right words or algorithms, but because they have learned how to turn on their own ability to reason, troubleshoot, think ... learning is non-linear, orthogonal, seemingly random at times, eg "WTF, where did that come from?" Learning is not about memorization or rote repitition, although those sub-tasks might help one BEGIN to learn -- learning is about being challenged and thinkering and applying concepts in different contexts.  

Literacy is not about reading individual words ... a first-grader can *sound out* a word, look it up and memorize a definition -- we might admire that exercise in vocabulary building, but we don't call that kind of thing literacy ... we don't really consider someone to be literate until they have developed a sense of context, composition and meaning.

Maybe we should try improving scientific literacy by in involving people in the peer review and commentary process [a la StackExchange reputation-driven fora] ... but beyond just commenting on papers ... people should also be involved in crowdsourcing the connections between papers ... and those connections should also be reputation-weighted.

The real question is ... how do we implement this?